from setuptools import setup, find_packages

with open("requirements.txt", "r") as f:
    requirements = f.read().splitlines()

setup(
    name="picoswitch",
    version="0.1",
    packages=find_packages(),
    insteall_requires=requirements,
    licensce="GPL-3",
)
