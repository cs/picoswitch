#!/usr/bin/env python3
from aiohttp import web
from aiohttp_swagger3 import SwaggerDocs, SwaggerUiSettings
import asyncio


from .picoswitch import Picoswitch


class PicoswitchInterface:
    def __init__(self, switch: Picoswitch):
        self._switch = switch

    async def get_status(self, request: web.Request) -> web.Response:
        """
        ---
        summary: get the current status of the device
        responses:
          '200':
            description: TODO
        """
        return web.json_response(dict(done=self._switch.done()))

    async def move(self, request: web.Request) -> web.Response:
        """
        ---
        summary: move a channel by the given distance
        requestBody:
          content:
            'application/json':
              schema:
                type: object
                properties:
                  board:
                    description: Number of the board to address
                    type: integer
                    minimum: 1
                    maximum: 4
                  channel:
                    description: Channel of the board to address
                    type: integer
                    minimum: 1
                    maximum: 12
                  distance:
                    description: Relative distance to move (negative for backwards)
                    type: integer
                required:
                  - board
                  - channel
                  - distance
                additionalProperties: false
              examples:
                moveForward:
                  summary: Move channel 5 on board 2 forward
                  value:
                    board: 2
                    channel: 5
                    distance: 500
        responses:
          '200':
            description: Output started to move as specified
          '400':
            description: Invalid channel
          '503':
            description: another movement is still running, try again later
        """
        if not self._switch.done():
            return web.json_response(
                dict(result="error", message="still moving"), status=503
            )

        params = await request.json()
        board = int(params["board"])
        channel = int(params["channel"])
        distance = params["distance"]

        asyncio.get_running_loop().run_in_executor(
            None, self._switch.move_relative, board, channel, distance
        )

        return web.json_response(dict(result="ok"))

    async def stop(self, request: web.Request) -> web.Response:
        """
        ---
        description: immediately stop any movement
        responses:
          '200':
            description: successfully stopped
        """
        self._switch.stop()
        return web.json_response(dict(result="ok"))

    async def get_config(self, request: web.Request) -> web.Response:
        """
        ---
        summary: get the current configuration entries
        responses:
          '200':
            description: the current config
        """
        return web.json_response(
            dict(acceleration=self._switch.acceleration, velocity=self._switch.velocity)
        )

    async def set_config(self, request: web.Request) -> web.Response:
        """
        ---
        summary: change the current configuration
        requestBody:
          content:
            'application/json':
              schema:
                type: object
                properties:
                  acceleration:
                    description: the acceleration value to set for all channels
                    type: integer
                    minimum: 1
                    maximum: 200000
                  velocity:
                    description: the velocity value to set for all channels
                    type: integer
                    minimum: 1
                    maximum: 2000
                additionalProperties: false
              examples:
                newVelocity:
                  summary: Change the velocity to 500
                  value:
                    velocity: 500
        responses:
          '200':
            description: configuration has been changed successfully
          '400':
            description: invalid config entry
        """
        data = await request.json()
        commands = dict(acceleration="AC", velocity="VA")
        for name, value in data.items():
            command = commands[name]
            self._switch.set_option(command, value)
        return web.json_response(dict(result="ok"))

    async def test_all(self, request: web.Request) -> web.Response:
        """
        ---
        summary: test all channels by cycling all relais once
        responses:
          '200':
            description: TODO
        """
        for board in range(1, 5):
            for pin in range(1, 13):
                self._switch.enable_output(board, pin)
                print(f"{board}, {pin}")
                await asyncio.sleep(0.5)
                self._switch.disable_output(board)


def init_app():
    switch = Picoswitch()
    app = web.Application()
    interface = PicoswitchInterface(switch)

    swagger = SwaggerDocs(
        app,
        swagger_ui_settings=SwaggerUiSettings(path="/docs/"),
        title="Picoswitch",
        version="1.0.0",
    )

    swagger.add_routes(
        [
            web.get("/status", interface.get_status),
            web.post("/move", interface.move),
            web.post("/stop", interface.stop),
            web.get("/config", interface.get_config),
            web.post("/config", interface.set_config),
            web.post("/test/all", interface.test_all),
        ]
    )

    return app


if __name__ == "__main__":
    import logging

    app = init_app()
    logging.basicConfig(level=logging.DEBUG)
    web.run_app(app)
