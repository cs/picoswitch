#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
from .newport import Controller
import Adafruit_BBIO.GPIO as GPIO


class Relaisboard(object):
    def __init__(self, pins):
        self._pins = pins
        for pin in self._pins.values():
            GPIO.setup(pin, GPIO.OUT)
        self._active = None

    def __del__(self):
        for pin in self._pins.values():
            GPIO.output(pin, GPIO.LOW)

    def set(self, output):
        pin = self._pins.get(output)

        if not pin:
            if output in self._pins.values():
                pin = output
            else:
                raise Exception("pin {} not found".format(p))

        if self._active == pin:
            return

        self.disable_all()
        GPIO.output(pin, GPIO.HIGH)
        self._active = pin

    def disable_all(self):
        for pin in self._pins.values():
            GPIO.output(pin, GPIO.LOW)
        self._active = None


pwrctl_pin = "P9_12"

default_pinmap = {
    1: {
        "name": "J4",
        "pins": {
            1: (9, 15),
            2: (9, 16),
            3: (8, 8),
            4: (8, 10),
            5: (8, 12),
            6: (8, 13),
            7: (8, 14),
            8: (8, 15),
            9: (9, 14),
            10: (8, 7),
            11: (8, 9),
            12: (8, 11),
        },
    },
    4: {
        "name": "J3",
        "pins": {
            1: (8, 16),
            2: (8, 18),
            3: (8, 38),
            4: (8, 40),
            5: (8, 42),
            6: (8, 44),
            7: (8, 45),
            8: (8, 46),
            9: (8, 17),
            10: (8, 39),
            11: (8, 41),
            12: (8, 43),
        },
    },
    3: {
        "name": "J2",
        "pins": {
            1: (9, 11),
            2: (9, 13),
            3: (9, 23),
            4: (9, 25),
            5: (9, 27),
            6: (9, 30),
            7: (9, 31),
            8: (9, 42),
            9: (9, 29),
            10: (9, 24),
            11: (9, 26),
            12: (9, 28),
        },
    },
    2: {
        "name": "J5",
        "pins": {
            1: (8, 26),
            2: (8, 28),
            3: (8, 30),
            4: (8, 32),
            5: (8, 34),
            6: (8, 35),
            7: (8, 36),
            8: (8, 37),
            9: (8, 27),
            10: (8, 29),
            11: (8, 31),
            12: (8, 33),
        },
    },
}


def translate_pins(pins):
    for i, pin in pins.items():
        pins[i] = "P{}_{}".format(*pin)
    return pins


class Picoswitch(object):
    def __init__(self, idProduct=0x4000, idVendor=0x104D, pinmap=default_pinmap):
        self._controller = Controller(idProduct=idProduct, idVendor=idVendor)
        self.initialise_controller()
        GPIO.setup(pwrctl_pin, GPIO.OUT)
        GPIO.output(pwrctl_pin, GPIO.LOW)
        self._boards = {
            i: Relaisboard(translate_pins(board["pins"])) for i, board in pinmap.items()
        }
        self.enable_power()

    def initialise_controller(self):
        for board in range(1, 5):
            # disable automatic motor detection. Does not work reliably over
            # our long cables
            self.command("{}ZZ1".format(board))
            # set motor type to standard
            self.command("{}QM3".format(board))

    def command(self, cmd):
        return self._controller.command(cmd)

    def enable_power(self):
        self.disable_all()
        GPIO.output(pwrctl_pin, GPIO.HIGH)

    def disable_power(self):
        GPIO.output(pwrctl_pin, GPIO.LOW)

    def enable_output(self, board, relais):
        self._boards[board].set(relais)

    def disable_output(self, board):
        self._boards[board].disable_all()

    def disable_all(self):
        for board in self._boards.values():
            board.disable_all()

    def move_relative(self, board, relais, distance):
        self.enable_output(board, relais)
        time.sleep(0.01)
        self._controller.command("{}PR{}".format(board, distance))
        self.wait_until_done(board)
        self.disable_output(board)

    def stop(self):
        self._controller.command("ST")

    def done(self, board=None):
        if not board:
            for board in self._boards.keys():
                if not self.done(board):
                    return False
            return True

        answer = self._controller.command("{}MD?".format(board))
        return answer[-1] != "0"

    def wait_until_done(self, board):
        while not self.done(board):
            time.sleep(0.05)

    @property
    def acceleration(self):
        return int(self.get_option("AC"))

    @acceleration.setter
    def acceleration(self, value):
        self.set_option("AC", value)

    @property
    def velocity(self):
        return int(self.get_option("VA"))

    @velocity.setter
    def velocity(self, value):
        self.set_option("VA", value)

    def get_option(self, option):
        return self._controller.command("1{}?".format(option))[2:]

    def set_option(self, option, value):
        for board in range(1, 5):
            self._controller.command("{}{}{}".format(board, option, value))


if __name__ == "__main__":
    switch = Picoswitch()
    from time import sleep

    while True:
        for board in range(1, 5):
            for pin in range(1, 13):
                switch.enable_output(board, pin)
                print("{}, {}".format(board, pin))
                sleep(1)
        switch.disable_output(board)
